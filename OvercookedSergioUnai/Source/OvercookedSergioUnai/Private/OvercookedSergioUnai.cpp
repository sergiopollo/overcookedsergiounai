// Copyright Epic Games, Inc. All Rights Reserved.

#include "OvercookedSergioUnai.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, OvercookedSergioUnai, "OvercookedSergioUnai" );

DEFINE_LOG_CATEGORY(LogOvercookedSergioUnai)
 