// Copyright Epic Games, Inc. All Rights Reserved.

#include "OvercookedSergioUnaiPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "OvercookedSergioUnaiCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputConfigData.h"
#include "Utils.h"

AOvercookedSergioUnaiPlayerController::AOvercookedSergioUnaiPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
}

void AOvercookedSergioUnaiPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
}

void AOvercookedSergioUnaiPlayerController::MoveCallback(const FInputActionValue& InputActionValue)
{
	const FVector2D MoveValue {InputActionValue.Get<FVector2D>()};
	const FRotator MoveRotator {0,this->GetControlRotation().Yaw,0};
	
	if(MoveValue.Y != 0.0f)
	{
		const FVector Dir {MoveRotator.RotateVector(FVector::ForwardVector)};
		this->GetCharacter()->AddMovementInput(Dir, MoveValue.Y);
	}
	if(MoveValue.X != 0.0f)
	{
		const FVector Dir {MoveRotator.RotateVector(FVector::RightVector)};
		this->GetCharacter()->AddMovementInput(Dir, MoveValue.X);
	}
	
}

void AOvercookedSergioUnaiPlayerController::OnGrabPressed(const FInputActionValue& InputActionValue)
{
	//ScreenPrintText("pressed grab: ");
	evOnGrabPressed.Broadcast();
}

void AOvercookedSergioUnaiPlayerController::OnInteractPressed(const FInputActionValue& InputActionValue)
{
	//ScreenPrintText("pressed interact: ");
	evOnInteractPressed.Broadcast();
}

void AOvercookedSergioUnaiPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
	{
		/*
		// Setup mouse input events
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Started, this, &AOvercookedSergioUnaiPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Triggered, this, &AOvercookedSergioUnaiPlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Completed, this, &AOvercookedSergioUnaiPlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Canceled, this, &AOvercookedSergioUnaiPlayerController::OnSetDestinationReleased);

		// Setup touch input events
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Started, this, &AOvercookedSergioUnaiPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Triggered, this, &AOvercookedSergioUnaiPlayerController::OnTouchTriggered);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Completed, this, &AOvercookedSergioUnaiPlayerController::OnTouchReleased);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Canceled, this, &AOvercookedSergioUnaiPlayerController::OnTouchReleased);
		*/

		EnhancedInputComponent->BindAction(mInputConfigData->InputMove, ETriggerEvent::Triggered, this, &AOvercookedSergioUnaiPlayerController::MoveCallback);
		EnhancedInputComponent->BindAction(mInputConfigData->InputGrab, ETriggerEvent::Started, this, &AOvercookedSergioUnaiPlayerController::OnGrabPressed);
		EnhancedInputComponent->BindAction(mInputConfigData->InputInteract, ETriggerEvent::Triggered, this, &AOvercookedSergioUnaiPlayerController::OnInteractPressed);
		
	}
}
/*
void AOvercookedSergioUnaiPlayerController::OnInputStarted()
{
	StopMovement();
}

// Triggered every frame when the input is held down
void AOvercookedSergioUnaiPlayerController::OnSetDestinationTriggered()
{
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	// We look for the location in the world where the player has pressed the input
	FHitResult Hit;
	bool bHitSuccessful = false;
	if (bIsTouch)
	{
		bHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, Hit);
	}
	else
	{
		bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
	}

	// If we hit a surface, cache the location
	if (bHitSuccessful)
	{
		CachedDestination = Hit.Location;
	}
	
	// Move towards mouse pointer or touch
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn != nullptr)
	{
		FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
		ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
	}
}

void AOvercookedSergioUnaiPlayerController::OnSetDestinationReleased()
{
	// If it was a short press
	if (FollowTime <= ShortPressThreshold)
	{
		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}

	FollowTime = 0.f;
}

// Triggered every frame when the input is held down
void AOvercookedSergioUnaiPlayerController::OnTouchTriggered()
{
	bIsTouch = true;
	OnSetDestinationTriggered();
}

void AOvercookedSergioUnaiPlayerController::OnTouchReleased()
{
	bIsTouch = false;
	OnSetDestinationReleased();
}
*/