// Copyright Epic Games, Inc. All Rights Reserved.

#include "OvercookedSergioUnaiGameMode.h"
#include "Kismet/KismetArrayLibrary.h"
#include "K2Node_GetDataTableRow.h"
#include "OvercookedSergioUnaiPlayerController.h"
#include "OvercookedSergioUnaiCharacter.h"
#include "UObject/ConstructorHelpers.h"

AOvercookedSergioUnaiGameMode::AOvercookedSergioUnaiGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AOvercookedSergioUnaiPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
	
}

void AOvercookedSergioUnaiGameMode::AttachNewOrder()
{
	if(mOrdersRecipes.Num()<MaxRecipes)
	{
		TArray<FName> RowNames=RecipeDB->GetRowNames();
		FName NewName = "";
		NewName = getRandomName(RowNames);
		static FString FindContext{FString("Searching for ").Append(NewName.ToString())};
		mOrdersRecipes.Add(RecipeDB->FindRow<FRecipeDataRow>(NewName,FindContext,true)->Recipetype);
		
	}
}

FName AOvercookedSergioUnaiGameMode::getRandomName(TArray<FName> NamesToIterate)
{
	int Index = rand() % NamesToIterate.Num();
	int actualindex=0;
	for(FName newname : NamesToIterate)
	{
		if(actualindex==Index)
		{
			return newname;
			break;
		}
		actualindex++;
	}
	return "";
}

