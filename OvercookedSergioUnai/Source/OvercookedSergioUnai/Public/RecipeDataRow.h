
#pragma once

#include "CoreMinimal.h"
#include "Ingredient.h"
#include "Engine/DataTable.h"
#include "Containers/Queue.h"
#include "RecipeDataRow.generated.h"

UENUM(BlueprintType)
enum class ERecipe: uint8
{
	NONE = 0 UMETA(DisplayName="None"),
	CHICKENHAM UMETA(DisplayName="Chicken Hamburger"),
	CHILETHAM UMETA(DisplayName="Chicken Hamburger with letuce"),
	CHICKEN UMETA(DisplayName="Double Chicken Hamburger")
};
USTRUCT(Blueprintable,BlueprintType)
struct FRecipeDataRow : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ERecipe Recipetype; 
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FIngredient> QIngredients;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float scorepunts;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TWeakObjectPtr<UTexture2D> Image {nullptr};
};
