#pragma once

#include "CoreMinimal.h"
#include "Ingredient.h"
#include "Recipe.generated.h"

USTRUCT(Blueprintable, BlueprintType)
struct FRecipe
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FIngredient> RequiredIngredients;

};
