// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "InputActionValue.h"
#include "OvercookedSergioUnaiPlayerController.generated.h"

/** Forward declaration to improve compiling times */
class UNiagaraSystem;
class UInputConfigData;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGrabPressed);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInteractPressed);

UCLASS()
class AOvercookedSergioUnaiPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AOvercookedSergioUnaiPlayerController();

	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputConfigData* mInputConfigData;	

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FGrabPressed evOnGrabPressed;
		
    UPROPERTY(BlueprintAssignable, BlueprintCallable)
        FInteractPressed evOnInteractPressed;

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	void MoveCallback(const FInputActionValue& InputActionValue);

	void OnGrabPressed(const FInputActionValue& InputActionValue);

	void OnInteractPressed(const FInputActionValue& InputActionValue);
	
	virtual void SetupInputComponent() override;
	
	// To add mapping context
	virtual void BeginPlay();

	/** Input handlers for SetDestination action. */
	void OnInputStarted();
	void OnSetDestinationTriggered();
	void OnSetDestinationReleased();
	void OnTouchTriggered();
	void OnTouchReleased();

private:
	FVector CachedDestination;

	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
};


