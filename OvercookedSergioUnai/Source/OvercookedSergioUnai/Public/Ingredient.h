#pragma once

#include "CoreMinimal.h"
#include "Ingredient.generated.h"

UENUM(BlueprintType)
enum class EIngredientType : uint8
{
	CHICKEN UMETA(DisplayName = "Chicken"),
	LETTUCE UMETA(DisplayName = "Lettuce"),
	BREAD UMETA(DisplayName = "Bread")
};

UENUM(BlueprintType)
enum class EIngredientState : uint8
{
	RAW UMETA(DisplayName = "Raw"),
	CUT UMETA(DisplayName = "Cut"),
	COOK UMETA(DisplayName = "Cook")
};

USTRUCT(Blueprintable, BlueprintType)
struct FIngredient
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EIngredientType myType {EIngredientType::CHICKEN};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EIngredientState currentState {EIngredientState::RAW};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		//TQueue<EIngredientState> IngredientStateQueue;
		TArray<EIngredientState> IngredientStateQueue;

};

