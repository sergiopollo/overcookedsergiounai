// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "RecipeDataRow.h"
#include "GameFramework/GameModeBase.h"
#include "OvercookedSergioUnaiGameMode.generated.h"

UCLASS(minimalapi)
class AOvercookedSergioUnaiGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOvercookedSergioUnaiGameMode();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int points;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<ERecipe> mOrdersRecipes;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UDataTable* RecipeDB;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int MaxRecipes;
	
	UFUNCTION(BlueprintCallable)
	void AttachNewOrder();
	
	UFUNCTION(BlueprintCallable)
	FName getRandomName(TArray<FName>Names);
};



